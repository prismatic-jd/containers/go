# Use the specified base image
FROM registry.gitlab.com/prismatic-jd/containers/asdf:0.11.3

# Set the argument for the CI_COMMIT_REF_NAME
ARG CI_COMMIT_REF_NAME

# Set the default shell to be used with the -e option to exit immediately if a 
# command exits with a non-zero status, and the -c option to read commands from 
# a string
SHELL ["/bin/bash", "-e", "-c"]

# Install golang using asdf, set the appropriate version based on the
#  CI_COMMIT_REF_NAME, and write it to .tool-versions file
RUN asdf plugin-add golang \
    && VERSION_CONDITION=$(if [ "$CI_COMMIT_REF_NAME" = "main" ]; then echo "latest"; else echo "$CI_COMMIT_REF_NAME"; fi) \
    && asdf install golang "$VERSION_CONDITION" \
    && echo "golang $(if [ "$CI_COMMIT_REF_NAME" = "main" ]; then asdf latest golang; else echo "$CI_COMMIT_REF_NAME"; fi)" > .tool-versions
